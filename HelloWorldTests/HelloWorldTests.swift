//
//  HelloWorldTests.swift
//  HelloWorldTests
//
//  Created by uics14 on 10/7/15.
//  Copyright © 2015 uics14. All rights reserved.
//

import UIKit
import XCTest

class HelloWorldTests: XCTestCase {
    // MARK: HelloWorld Tests

    // Tests to confirm that the player initializer returns when no name is provided.
    func testPlayerInitialization() {
        
        // Success case.
        let potentialPlayer = Player(name: "Phil")
        XCTAssertNotNil(potentialPlayer)
        
        // Failure case/
        let noName = Player(name: "")
        XCTAssertNil(noName, "Empty name is invalid")
        
    }

}
