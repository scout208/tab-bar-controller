# README #

This project is a random three tab app to get practice with Tab Bar and Navigation controllers, Table View, and storing information in NSUserDefaults using Swift.

At the top level of the app there are three tabs:

1. The first tab presents a table view that lists known users/players/registered people. You can add a new person to the list. Selecting a player shows more details about the player. On the details screen, there is:
    * a button to set the person as "active user" of the overall app
    * the last value on the calculator's display (tab 2) when this person most recently used the calculator (even if the app was completely closed/terminated after their most recent use)
    * the highest score the person has ever achieved on the tapping game (tab 3)
2. The second tab presents a calculator
3. The third tab presents a very simple game. When the button is pressed, a timer starts that lasts 10 seconds. During that 10 seconds, the number of taps on the screen is counted. When the 10 seconds are finished, the total number of taps is displayed as the game score.