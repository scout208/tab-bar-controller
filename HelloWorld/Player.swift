//
//  Player.swift
//  HelloWorld
//
//  Created by uics14 on 10/8/15.
//  Copyright © 2015 uics14. All rights reserved.
//

import UIKit

class Player {
    // MARK: Properties
    
    var name: String
    var active: Bool
    var calcLastValue: Double
    var highScore: Int
    var lastActive: String
    
    // MARK: Initialization
    
    init?(name: String) {
        // Initialize stored properties
        self.name = name
        active = false
        calcLastValue = 0.0
        highScore = 0
        lastActive = ""
        
        // Initialization should fail if there is no name
        if name.isEmpty {
            return nil
        }
    }

}