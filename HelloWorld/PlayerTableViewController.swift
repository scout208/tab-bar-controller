//
//  PlayerTableViewController.swift
//  HelloWorld
//
//  Created by uics14 on 10/8/15.
//  Copyright © 2015 uics14. All rights reserved.
//

import UIKit

class PlayerTableViewController: UITableViewController {
    
    let playersKeyConstant = "playersKey"
    
    // MARK: Properties
    var players = [Player]()
    var activePlayer: Player?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    // MARK: - Table view data source

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return players.count
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        // Table view cells are reused and should be dequeued using a cell identifier.
        let cellIdentifier = "PlayerTableViewCell"
        let cell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier, forIndexPath: indexPath) as! PlayerTableViewCell

        // Fetches the appropriate player for the data source layout
        let player = players[indexPath.row]
        
        cell.nameLabel.text = player.name
        
        return cell
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */
    
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "ShowDetail" {
            let playerDetailViewController = segue.destinationViewController as! PlayerViewController
            
            // Get the cell that generated this segue.
            if let selectedPlayerCell = sender as? PlayerTableViewCell {
                let indexPath = tableView.indexPathForCell(selectedPlayerCell)!
                let selectedPlayer = players[indexPath.row]
                playerDetailViewController.player = selectedPlayer
                playerDetailViewController.players = players
            }
        }
        else if segue.identifier == "AddItem" {
            print("Adding new player.")
        }
    }
    
    @IBAction func unwindToPlayerList(sender: UIStoryboardSegue) {
        if let sourceViewController = sender.sourceViewController as? NewPlayerViewController, player = sourceViewController.player {
            // Add a new player.
            let newIndexPath = NSIndexPath(forRow: players.count, inSection: 0)
            players.append(player)
            tableView.insertRowsAtIndexPaths([newIndexPath], withRowAnimation: .Bottom)
        }
    }

}
