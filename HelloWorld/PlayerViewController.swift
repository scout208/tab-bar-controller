//
//  PlayerViewController.swift
//  HelloWorld
//
//  Created by uics14 on 10/10/15.
//  Copyright © 2015 uics14. All rights reserved.
//

import UIKit

class PlayerViewController: UIViewController, UINavigationControllerDelegate {

    /*
    This value is passed by `PlayerTableViewController` in `prepareForSegue(_:sender:)`
    */
    var player: Player?
    var players: [Player]?
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var activeUserSwitch: UISwitch!
    @IBOutlet weak var calcLastValueLabel: UILabel!
    @IBOutlet weak var highscoreLabel: UILabel!
    @IBOutlet weak var lastActiveLabel: UILabel!
    
    @IBAction func activeUserSwitchPressed(sender: UISwitch) {
        if activeUserSwitch.on {
            // Set all other players to inactive
            if let players = players {
                for playa in players {
                    if playa.active {
                        playa.active = false
                        playa.lastActive = NSDateFormatter.localizedStringFromDate(NSDate(), dateStyle: .MediumStyle, timeStyle: .ShortStyle)
                    }
                }
            }
            
            // Set the current player to active and update the label
            player?.active = true
            player?.lastActive = "Currently active"
            lastActiveLabel.text = player?.lastActive
            
            // Set the active player in the app
            let tbvc = tabBarController as! TabBarController
            tbvc.activePlayer = player
        }
        else {
            player?.active = false
            player?.lastActive = NSDateFormatter.localizedStringFromDate(NSDate(), dateStyle: .MediumStyle, timeStyle: .ShortStyle)
            lastActiveLabel.text = player?.lastActive
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do the initial setup after loading the view.
        if let player = player {
            navigationItem.title = player.name
            nameLabel.text   = player.name
            /*calcLastValueLabel.text = "\(player.calcLastValue)"
            highscoreLabel.text = "\(player.highScore)"
            lastActiveLabel.text = player.lastActive*/
        }

    }
    
    override func viewWillAppear(animated: Bool) {
        if let player = player {
            calcLastValueLabel.text = "\(player.calcLastValue)"
            highscoreLabel.text = "\(player.highScore)"
            
            activeUserSwitch.on = player.active
            lastActiveLabel.text = player.lastActive
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}
