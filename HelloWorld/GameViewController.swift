//
//  GameViewController.swift
//  HelloWorld
//
//  Created by uics14 on 10/12/15.
//  Copyright © 2015 uics14. All rights reserved.
//

import UIKit

class GameViewController: UIViewController {
    
    // MARK: - Properties
    
    var timer = NSTimer()
    let timeInterval:NSTimeInterval = 10.0
    var counter = 0
    var started = false
    var score = 0
    var highScore = 0
    
    var activePlayer: Player?
    
    // MARK: - Outlets
    
    @IBOutlet weak var counterLabel: UILabel!
    @IBOutlet weak var scoreLabel: UILabel!
    @IBOutlet var tapRecognizer: UITapGestureRecognizer!
    @IBOutlet weak var button: UIButton!

    // MARK: - Actions
    
    @IBAction func startTimer(sender: UIButton) {
        timer = NSTimer.scheduledTimerWithTimeInterval(timeInterval,
            target: self,
            selector: "timerDidEnd:",
            userInfo: "Time's Up!",
            repeats: false)
        started = true
        
        button.enabled = false
    }
    
    @IBAction func tapHappened(sender: UITapGestureRecognizer) {
        if started {
            counter++
            counterLabel.text = "\(counter)"
            print(counter)
        }
    }
    
    @IBAction func reset(sender: UIBarButtonItem) {
        counter = 0
        score = 0
        highScore = 0
        
        counterLabel.text = "\(counter)"
        scoreLabel.text = "\(highScore)"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        button.setTitle("Tap away!", forState: .Disabled)
    }
    
    override func viewWillDisappear(animated: Bool) {
        let tbvc = tabBarController as! TabBarController
        if tbvc.activePlayer?.highScore < highScore {
            tbvc.activePlayer?.highScore = highScore
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Instance Methods
    func timerDidEnd(timer:NSTimer){
        
        // Get the score
        score = counter
        if score > highScore {
            highScore = score
            scoreLabel.text = "\(highScore)"
        }
        
        // Show alert
        let timesUpAlert = UIAlertController(title: "Time's Up!!", message: "Score: \(score)", preferredStyle: UIAlertControllerStyle.Alert)
        timesUpAlert.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.Default,handler: nil))
        
        self.presentViewController(timesUpAlert, animated: true, completion: nil)
        
        timer.invalidate()
        
        // Reset everything for next time
        counter = 0
        score = 0
        counterLabel.text = "\(counter)"
        started = false
        button.enabled = true
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
